# SOFTWARE NAME
Brief summary of the project

## Requirements
* list 
* of 
* software 
* requirements 

## Installation
Installation instructions

## Usage
Quickstart guide with example(s)

`launchme.ext -o1 option -o2 anotherOption`

More in-depth guide if needed

## Maintainer
maintainer@provider.com

Write to this address if you want to be added as a collaborator.

## Acknowledgement

This project has received funding from the European Union’s Horizon 2020
research and innovation programme under the Marie Skłodowska-Curie grant
agreement No 823886<br>

Software name is part of Work Package X; Deliverable Y<br>

# Project tags

Remember to tag your project! Go to Settings and specify one type among
* Data
* Software
* Deliverable

Then, use the right avatar. Avatars are included in this project.
